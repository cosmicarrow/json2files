# JSON to Files

Generates files and project structure based on a JSON configuration.

## Install

```bash
sudo npm i -g json-to-files
```

## Update

```bash
sudo npm update -g
```

## Usage

```bash
json-to-files --json structure.json --output my_project_path --templates ./path/to/templates
```

## JSON Structure Example

```json
{
  "structure":{
    "src": {
      "files": [
        {
          "name": "main.ts",
          "template": "defaultTs.ejs",
          "action": "overwrite",
          "content": {
            "title": "Main File",
            "description": "This is the main TypeScript file"
          }
        },
        {
          "name": "config.json",
          "template": "configJson.ejs",
          "action": "skip",
          "content": {
            "title": "Configuration File",
            "description": "This is a configuration file"
          }
        }
      ],
      "subdir": {
        "files": [
          {
            "name": "readme.md",
            "template": "readme.ejs",
            "content": {
              "title": "Subdirectory File",
              "description": "This file is in a subdirectory"
            }
          }
        ]
      },
      "subdir2": {
        "files": [
          {
            "name": "subfile2.ts",
            "template": "defaultTs.ejs",
            "action": "overwrite",
            "content": {
              "title": "Subdirector2 File",
              "description": "This file is in a subdirectory2"
            }
          }
        ]
      }
    },
    "files": [
      {
        "name": "package.json",
        "template": "packageJson.ejs",
        "action": "overwrite",
        "content": {
          "title": "Configuration File",
          "description": "This is a configuration file"
        }
      }
    ]
  },
  
  "config":{
    "project_name": "json-to-files",
    "version": "1.1.0",
    "description": "Generates files and project structure based on a JSON configuration",
    "author": "your-name",

    "package.json":{
      "scripts": {
        "build": "tsc",
        "start": "node dist/index.js"
      },
      "keywords": [
        "json",
        "files",
        "typescript"
      ],
      "license": "MIT",
      "dependencies": {
        "ejs": "^3.1.10",
        "fs-extra": "^11.2.0",
        "path": "^0.12.7",
        "yargs": "^17.7.2"
      },
      "devDependencies": {
        "@types/ejs": "^3.1.5",
        "@types/node": "^20.12.12",
        "@types/yargs": "^17.0.32",
        "ts-node": "^10.9.2",
        "typescript": "^4.9.5"
      }
    }
  }
}

```