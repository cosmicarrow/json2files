#!/usr/bin/env node

import yargs from 'yargs';
import { hideBin } from 'yargs/helpers';

import { essentialExistence, createFilesAndDirs, getJson, transformInput, setDefaultParam, getPath } from './lib'
import { Directory } from './interfaces';

const argv = yargs(hideBin(process.argv))
    .option('structure-json', {
        alias: 's',
        type: 'string',
        description: 'Path to the JSON file containing the project structure',
    })
    .option('output', {
        alias: 'o',
        type: 'string',
        description: 'Path to the output directory where files will be generated',
        default: 'my_project',
    })
    .option('templates', {
        alias: 't',
        type: 'string',
        description: 'Path to the directory containing the template files',
    })
    .option('config-folder', {
        alias: 'f',
        type: 'string',
        description: 'Path to the directory of config files',
        default: './',
    })
    .option('config-json', {
        alias: 'c',
        type: 'string',
        description: 'Path to the directory of config files',
    })
    .parseSync();

argv.templates = setDefaultParam(argv.templates, argv.configFolder);
argv.structureJson = setDefaultParam(argv.structureJson, argv.configFolder, '/structure.json');
argv.configJson = setDefaultParam(argv.configJson, argv.configFolder, '/config.json');

const outputDirectoryPath = getPath(argv.output);
const structureJsonFilePath = getPath(argv.structureJson);
const templatesDirectoryPath = getPath(argv.templates);
const configFilePath = getPath(argv.configJson);
essentialExistence(structureJsonFilePath, 'The structure json file does not exist');
essentialExistence(templatesDirectoryPath, 'The templates directory does not exist');

const config = getJson(configFilePath);
const structure:Directory = getJson(structureJsonFilePath);

createFilesAndDirs(outputDirectoryPath, templatesDirectoryPath, structure, config, transformInput(structure));

console.log('Generated files based on the JSON structure.');
