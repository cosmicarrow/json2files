interface File {
    name?: string;
    action?: 'overwrite' | 'skip';
    template?: string;
    content: Record<string, any>;
}

interface Directory {
    files?: File[];
    [subdir: string]: any;
}

interface OutputFormat {
    [key: string]: any;
}

export { File, Directory, OutputFormat }