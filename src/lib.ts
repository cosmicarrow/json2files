
import { File, Directory, OutputFormat } from './interfaces'
import * as fs from 'fs';
import * as path from 'path';
import ejs from 'ejs';

function getPath(input?:string){
    return path.resolve(process.cwd(), removeLeadingSpace(input));
}

function setDefaultParam(value:string | undefined, argConfigFolder:string, defaultFile?:string){
    const configFolderPath = path.resolve(process.cwd(), removeLeadingSpace(argConfigFolder));
    if(!value){
        try{
            if(fs.lstatSync(configFolderPath).isDirectory()) value = argConfigFolder + (defaultFile ?? '');
        }catch(e){
            console.log(`The config folder is invalid.\nPath: ${configFolderPath}\n`);
        }
    }
    return value;
}

function getJson(configPath:string){
    let json:any;
    if(fs.existsSync(configPath)) json = JSON.parse(fs.readFileSync(configPath, 'utf-8'));
    return json;
}

function removeLeadingSpace(input?:string): string {
    return (input?.startsWith(' '))? input.slice(1): input ?? '';
}

function essentialExistence(path:string, message:string){
    if(!fs.existsSync(path)){
        console.log(`${message}. Path: ${path}`);
        console.log('Stopping script.')
        process.exit(0);
    }
}

function transformDirectory(inputDir: Directory): OutputFormat {
    const result: OutputFormat = {};

    if(inputDir.files){
        for(const file of inputDir.files) if(file.name) result[file.name] = file.content;
    }

    for(const key in inputDir){
        if(key !== 'files' && typeof inputDir[key] === 'object') result[key] = transformDirectory(inputDir[key]);
    }

    return result;
}

function transformInput(input: Directory): OutputFormat {
    const output: OutputFormat = {};

    for (const key in input) {
        if((key !== 'files' && typeof input[key] === 'object')) output[key] = transformDirectory(input[key]);
        else for(const file of input[key]) output[file.name] = file.content;
    }

    return output;
}

function skipFile(filePath:string, file:File){
    file.action ??= 'overwrite'
    if(fs.existsSync(filePath) && file.action === 'skip') {
        console.log(`Skipping file creation for ${filePath}\nObject: ${JSON.stringify(file)}\n`);
        return true;
    }
}

function getTemplate(templatePath:string, file:File){
    let template = '';

    if(!fs.existsSync(templatePath)) console.log(`The template file does not exist.\nPath: ${templatePath}\nObject: ${JSON.stringify(file)}\n`);
    else if(file.template) template = fs.readFileSync(templatePath, 'utf-8');

    return template;
}

function writeRenderedContent(basePath:string, filePath:string, renderedContent:string, file:File){
    if(!fs.existsSync(basePath)) console.log(`The directory does not exist.\nPath: ${basePath}\n`);
    else if(!file.name) console.log(`The file name does not exist.\nObject: ${JSON.stringify(file)}\n`);
    else{
        try{
            fs.writeFileSync(filePath, renderedContent); 
        } catch(e){
            console.log(`The file name has an error. File name: ${file.name}.\nObject: ${JSON.stringify(file)}\n`);
        }
    }
    
}

function createFilesAndDirs(basePath: string, templatesDirectoryPath:string, structure:Directory, config:any, structureObject:any){
    if (!fs.existsSync(basePath)) fs.mkdirSync(basePath, { recursive: true });

    if (structure.files) {
        structure.files.forEach(file => {
            const filePath = path.resolve(process.cwd(), basePath, file.name ?? '');
            if(skipFile(filePath, file)) return;

            const templatePath = path.resolve(process.cwd(), templatesDirectoryPath, file.template ?? '');
            const template = getTemplate(templatePath, file);
            const renderedContent = ejs.render(template, { ...file.content, "structure": structureObject, "config": config });

            writeRenderedContent(basePath, filePath, renderedContent, file);
        });
    }

    Object.keys(structure).forEach(key => {
        if (key !== 'files') {
            const subPath = path.join(basePath, key);
            if(structure) createFilesAndDirs(subPath,  templatesDirectoryPath, structure[key] as Directory, config, structureObject);
        }
    });
};

export { 
    removeLeadingSpace, 
    essentialExistence, 
    transformInput, 
    createFilesAndDirs, 
    getJson,
    setDefaultParam,
    getPath
}